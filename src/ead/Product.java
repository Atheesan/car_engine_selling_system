package ead;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

/**
 * This class defines the methods for basic operations of create, update & retrieve
 * for the product entity
 * 
 * @author
 *
 */
public class Product {

	/**
	 * Update or create the product
	 * 
	 * @param name
	 *          : name of the product
	 * @param description
	 *          : description
	 * @return updated product
	 */
  public static void createOrUpdateProduct(String name, String description) {
    Entity product = getProduct(name);
    if (product == null) {
      product = new Entity("Product", name);
    }
    product.setProperty("description", description);
    Util.persistEntity(product);
  }

	/**
	 * Return all the products
	 * 
	 * @param kind
	 *          : of kind product
	 * @return products
	 */
  public static Iterable<Entity> getAllProducts(String kind) {
    return Util.listEntities(kind, null, null);
  }

	/**
	 * Get product entity
	 * 
	 * @param name
	 *          : name of the product
	 * @return: product entity
	 */
  public static Entity getProduct(String name) {
    Key key = KeyFactory.createKey("Product", name);
    return Util.findEntity(key);
  }
}
