package ead;

import java.util.Iterator;
import java.util.ResourceBundle;

import javax.servlet.ServletException;

/**
 * This servlet pre populates some data into customers and products
 * 
 * @author
 */
@SuppressWarnings("serial")
public class PrePopulateDataServlet extends BaseServlet {

  @Override
  public void init() throws ServletException {
    populateProduct();
    populateCustomer();
  }

  private void populateProduct() {
    ResourceBundle rb = ResourceBundle.getBundle("Product");
    Iterator<String> keys = rb.keySet().iterator();
    String prop;
    while(keys.hasNext()){
      prop = rb.getString(keys.next());
      String values[] = prop.split(",");
      Product.createOrUpdateProduct(values[0], values[1]);
    }
  }

  private void populateCustomer() {
    ResourceBundle rb = ResourceBundle.getBundle("Customer");
    Iterator<String> keys = rb.keySet().iterator();
    String prop;
    while(keys.hasNext()){
      prop = rb.getString(keys.next());
      String values[] = prop.split(",");
      Customer.createOrUpdateCustomer(values[0], values[1],  values[2],  values[3],  values[4],  values[5], 
              values[6],  values[7],  values[8]);
    }
  }
}
